import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavListComponent } from './nav-list/nav-list.component';
import {RouterModule} from '@angular/router';
import {MatButtonModule} from '@angular/material/button';



@NgModule({
  declarations: [NavListComponent],
  imports: [
    RouterModule,
    CommonModule,
    MatButtonModule
  ],
  exports: [
    MatButtonModule,
    CommonModule,
    NavListComponent
  ]
})
export class SharedModule { }

import {Component, Input} from '@angular/core';
import {RouteItemInterface} from '../interfaces';

@Component({
  selector: 'app-nav-list',
  templateUrl: './nav-list.component.html',
  styleUrls: ['./nav-list.component.scss']
})
export class NavListComponent {
  @Input() routeItems: RouteItemInterface[];
}

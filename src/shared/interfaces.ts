export interface RouteItemInterface {
  link: string;
  label: string;
}

export interface TodoInterface {
  label: string;
  finished: boolean;
}

export interface ContactDetailsInterface {
  phone: string;
  email: string;
}

export interface CustomerDetailsInterface {
  firstName: string;
  lastName: string;
  todoList: TodoInterface[];
  suggestions: string[];
  contactDetails: ContactDetailsInterface;
}

export interface LegalInformationInterface {
  passportNumber: string;
  avatar: string;
}

export interface TransactionHistoryInterface {
  currency: string;
  amount: number;
  date: Date | number;
}

export interface AccountSettingsInterface {
  sms: boolean;
  email: boolean;
  webNotification: boolean;
  [key: string]: boolean;
}

export interface AccountDetailsInterface {
  legalInformation: LegalInformationInterface;
  transactionHistory: TransactionHistoryInterface[];
  settings: AccountSettingsInterface;
}

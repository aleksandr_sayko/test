import { Component } from '@angular/core';
import {FormBuilder, ValidatorFn, ValidationErrors, FormGroup} from '@angular/forms';
import { MatRadioChange } from '@angular/material/radio';

const dynamicFormValidator: ValidatorFn = (form: FormGroup): ValidationErrors | null => {
  const {isSent, file, date, passport, urgent, deadline} = form.controls;

  let result = null;

  if (isSent.value === true) {
    if (!file.value) {
      result = {file: true};
    }

    if (!date.value) {
      result = result || {};
      result = {...result, date: true};
    }
  } else if (isSent.value === false) {
    if (!passport.value) {
      result = {passport: true};
    }

    if (urgent.value === true) {
      if (!deadline.value) {
        result = result || {};
        result = {...result, deadline: true};
      }
    } else if (urgent.value !== false) {
      result = result || {};
      result = {...result, urgent: true};
    }
  } else {
    result = {isSent: true};
  }

  return result;
};

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss']
})
export class DynamicFormComponent {
  constructor(private formBuilder: FormBuilder) {
  }

  form = this.formBuilder.group({
    isSent: [''],
    file: [''],
    date: [''],
    comment: [''],
    passport: [''],
    urgent: [''],
    deadline: ['']
  }, {validators: dynamicFormValidator});

  passportOptions: string[] = ['321456', '321456'];

  onSubmit = () => {
    console.log(this.form.value);
  }

  onIsSentChange = (event: MatRadioChange) => {
    // need to reset value because of browser security

    if (event.value) {
      this.form.get('file').setValue('');
    }
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GridComponent} from './grid/grid.component';
import {AccountDetailsComponent} from './account-details/account-details.component';
import {CustomerDetailsComponent} from './customer-details/customer-details.component';

const routes: Routes = [
  {
    path: 'grid',
    component: GridComponent,
    children: [
      {
        path: 'customer-details',
        component: CustomerDetailsComponent
      },
      {
        path: 'account-details',
        component: AccountDetailsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GridRoutingModule { }

import { Component } from '@angular/core';
import {CustomerDetailsInterface} from '../../../shared/interfaces';

const customer: CustomerDetailsInterface = {
  contactDetails: {
    email: 'aleksandr@test.com',
    phone: '0349957234'
  },
  firstName: 'aleksandr',
  lastName: 'sayko',
  suggestions: [
    'https://cdn.pixabay.com/photo/2020/09/04/20/09/cartoon-5544856_960_720.jpg',
    'https://cdn.pixabay.com/photo/2020/09/04/20/09/cartoon-5544856_960_720.jpg',
    'https://cdn.pixabay.com/photo/2020/09/04/20/09/cartoon-5544856_960_720.jpg',
    'https://cdn.pixabay.com/photo/2020/09/04/20/09/cartoon-5544856_960_720.jpg',
  ],
  todoList: [
    {
      finished: true,
      label: 'finish grid'
    },
    {
      finished: false,
      label: 'finish form'
    },
  ]
};

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.scss']
})
export class CustomerDetailsComponent {
  customer = customer;
}

import { Component } from '@angular/core';
import {AccountDetailsInterface} from '../../../shared/interfaces';
import {NgForm} from '@angular/forms';

const account: AccountDetailsInterface = {
  legalInformation: {
    avatar: 'https://www.flaticon.com/svg/static/icons/svg/147/147144.svg',
    passportNumber: '123456'
  },
  settings: {
    email: true,
    sms: false,
    webNotification: false
  },
  transactionHistory: [
    {
      amount: 1000,
      currency: 'USD',
      date: Date.now()
    },
    {
      amount: 2000,
      currency: 'EUR',
      date: Date.now()
    }
  ]
};

@Component({
  selector: 'app-account-details',
  templateUrl: './account-details.component.html',
  styleUrls: ['./account-details.component.scss']
})
export class AccountDetailsComponent {
  account: AccountDetailsInterface = account;

  onSubmit = (form: NgForm) => {
    console.log(form);
  }
}

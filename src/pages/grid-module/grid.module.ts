import {NgModule} from '@angular/core';
import {GridComponent} from './grid/grid.component';
import {GridRoutingModule} from './grid-routing.module';
import { AccountDetailsComponent } from './account-details/account-details.component';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import {SharedModule} from '../../shared/shared.module';
import { SidebarComponent } from './sidebar/sidebar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatTableModule} from '@angular/material/table';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [GridComponent, AccountDetailsComponent, CustomerDetailsComponent, SidebarComponent, DashboardComponent],
  imports: [
    SharedModule,
    GridRoutingModule,
    MatCheckboxModule,
    MatTableModule,
    FormsModule
  ],
  exports: [GridRoutingModule]
})
export class GridModule {}

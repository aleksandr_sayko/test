import { Component } from '@angular/core';
import {RouteItemInterface} from '../../../shared/interfaces';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent {

  routeItems: RouteItemInterface[] = [
    {label: 'customer details', link: '/grid/customer-details'},
    {label: 'account details', link: '/grid/account-details'},
  ];
}

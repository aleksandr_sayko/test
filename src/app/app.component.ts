import {Component} from '@angular/core';
import {RouteItemInterface} from '../shared/interfaces';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  routeItems: RouteItemInterface[] = [
    {label: 'Grid', link: '/grid'},
    {label: 'Form', link: '/form'},
  ];
}
